import tables
import unittest

import ../src/therapist

suite "Test multiple arguments":
    setup:
        let prolog="nargs - utility that allows you to pass arguments to another command"
        
        let spec = (
            command: newStringArg("<command>", "Command", "The program to run"),
            args: newStringArg("<args>", "Argument", "Argument for the program to run", optional=true),
            verbose: newCountArg("-v, --verbose", "Run command in verbose mode"),
            help: newHelpArg()
        )

    test "No options needed":
        let (success, _) = spec.parseOrMessage(prolog, args="cat", command="nargs")
        require(success)
        require(spec.command.seen)
        require(not spec.args.seen)

    test "Help":
        let (success, message) = spec.parseOrMessage(prolog, args="--help", command="nargs")
        require(success)
        let expected = """
nargs - utility that allows you to pass arguments to another command

Usage:
  nargs <command> [<args>]
  nargs (-h | --help)

Arguments:
  <command>         Command
  <args>            Argument

Options:
  -v, --verbose...  Run command in verbose mode
  -h, --help        Show help message"""
        require(message.get==expected)

    test "Options needed with separator allowed":
        let (success, message) = spec.parseOrMessage(prolog, args="-- tar -cvzf", command="nargs")
        if not success:
            echo message.get
        require(success)
        require(spec.command.seen)
        require(spec.args.seen)
        require(spec.args.value=="-cvzf")

    test "Extra options":
        let extra = spec.parseWithExtras(prolog, args="tar --verbose -cvzf --ratio=9 file1 file2", command="nargs")
        require("-c" in extra.extra_options)
        require("-z" in extra.extra_options)
        require(spec.verbose.seen)
        require(spec.verbose.count==2)
        require("-f" in extra.extra_options)
        require("--ratio" in extra.extra_options)
        let ratio = extra.extra_options["--ratio"]
        require(StringArg(ratio).value=="9")
        require(extra.extra_args.values == @["file2"])
